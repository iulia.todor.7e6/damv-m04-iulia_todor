<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html" indent="yes"/>
    <xsl:template match="Videojuegos">

        <h2>Videojuegos</h2>
        <xsl:for-each select="Videojuego">
            <article>
                <img>
                    <xsl:if test="InfoGeneral/Foto">
                        <xsl:attribute name="src">images/<xsl:value-of select="InfoGeneral/Foto"></xsl:value-of>
                        </xsl:attribute>
                        <xsl:attribute name="alt">
                            <xsl:value-of select="Nombre"></xsl:value-of>
                        </xsl:attribute>
                    </xsl:if>
                </img>
                <h3>
                    <xsl:value-of select="Nombre"/>
                </h3>

                <p class="description">
                    <xsl:value-of select="InfoGeneral/Descripcion"/>
                </p>
                <div class="caracteristicas">
                    <p>Género: <xsl:value-of select="Genero"></xsl:value-of>
                        <xsl:value-of select="Genero/@subGen"></xsl:value-of>
                    </p>
                    <p>Plataforma: <xsl:value-of select="Plataformas/Plataforma"></xsl:value-of>
                    </p>
                    <p>Fecha de salida: <xsl:value-of select="Plataformas/Plataforma/@fechaSalida"></xsl:value-of>
                    </p>
                </div>
                <div class="Masinfo">
                    <a href="" target="_blank">Más info</a>
                </div>
            </article>
        </xsl:for-each>


    </xsl:template>

</xsl:stylesheet>