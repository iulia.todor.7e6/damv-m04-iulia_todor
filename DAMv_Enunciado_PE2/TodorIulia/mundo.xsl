<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="xml" indent="yes"/>
    <xsl:template match="mundo">
        <html>
            <head>
                <link rel="stylesheet" type="text/css" href="mundo.css"/>
            </head>

            <body>
                <table class="africa">
                    <caption>África</caption>
                    <tr>
                        <th>Bandera</th>
                        <th>Pais</th>
                        <th>Gobierno</th>
                        <th>Capital</th>
                    </tr>
                    <xsl:apply-templates select="continente[1]/paises"/>
                </table>

                <table class="europe">
                    <caption>Europe</caption>
                    <tr>
                        <th>Bandera</th>
                        <th>Pais</th>
                        <th>Gobierno</th>
                        <th>Capital</th>
                    </tr>
                    <xsl:apply-templates select="continente[2]/paises"/>
                </table>

                <table class="oceania">
                    <caption>Oceanía</caption>
                    <tr>
                        <th>Bandera</th>
                        <th>Pais</th>
                        <th>Gobierno</th>
                        <th>Capital</th>
                    </tr>
                    <xsl:apply-templates select="continente[3]/paises"/>
                </table>

                <table class="asia">
                    <caption>Asia</caption>
                    <tr>
                        <th>Bandera</th>
                        <th>Pais</th>
                        <th>Gobierno</th>
                        <th>Capital</th>
                    </tr>
                    <xsl:apply-templates select="continente[4]/paises"/>
                </table>

                <table class="america">
                    <caption>America</caption>
                    <tr>
                        <th>Bandera</th>
                        <th>Pais</th>
                        <th>Gobierno</th>
                        <th>Capital</th>
                    </tr>
                    <xsl:apply-templates select="continente[5]/paises"/>
                </table>


            </body>
        </html>
    </xsl:template>

    <xsl:template match="continente[1]/paises">
        <xsl:for-each select="pais">
            <xsl:sort select="nombre" order="ascending"/>
            <tr>
                <td>
                    <img>
                        <xsl:if test="foto">
                            <xsl:attribute name="src">img/<xsl:value-of select="foto"></xsl:value-of>
                            </xsl:attribute>
                            <xsl:attribute name="alt">
                                <xsl:value-of select="nombre"></xsl:value-of>
                            </xsl:attribute>
                        </xsl:if>
                    </img>
                </td>

                <td>
                    <xsl:value-of select="nombre"/>
                </td>

                <xsl:choose>
                    <xsl:when test="nombre[@gobierno='monarquia']">
                        <td style="background-color:yellow;">
                            <xsl:value-of select="nombre/@gobierno"/>
                        </td>
                    </xsl:when>

                    <xsl:when test="nombre[@gobierno='dictadura']">
                        <td style="background-color:red;">
                            <xsl:value-of select="nombre/@gobierno"/>
                        </td>
                    </xsl:when>

                    <xsl:otherwise>
                        <td>
                            <xsl:value-of select="nombre/@gobierno"/>
                        </td>
                    </xsl:otherwise>
                </xsl:choose>
                <td>
                    <xsl:value-of select="capital"/>
                </td>

            </tr>
        </xsl:for-each>

    </xsl:template>

    <xsl:template match="continente[2]/paises">

        <xsl:for-each select="pais">
            <xsl:sort select="nombre" order="ascending"/>
            <tr>

                <td>
                    <img>
                        <xsl:if test="foto">
                            <xsl:attribute name="src">img/<xsl:value-of select="foto"></xsl:value-of>
                            </xsl:attribute>
                            <xsl:attribute name="alt">
                                <xsl:value-of select="nombre"></xsl:value-of>
                            </xsl:attribute>
                        </xsl:if>
                    </img>
                </td>

                <td>
                    <xsl:value-of select="nombre"/>
                </td>

                <xsl:choose>
                    <xsl:when test="nombre[@gobierno='monarquia']">
                        <td style="background-color:yellow;">
                            <xsl:value-of select="nombre/@gobierno"/>
                        </td>
                    </xsl:when>

                    <xsl:when test="nombre[@gobierno='dictadura']">
                        <td style="background-color:red;">
                            <xsl:value-of select="nombre/@gobierno"/>
                        </td>
                    </xsl:when>

                    <xsl:otherwise>
                        <td>
                            <xsl:value-of select="nombre/@gobierno"/>
                        </td>
                    </xsl:otherwise>
                </xsl:choose>
                <td>
                    <xsl:value-of select="capital"/>
                </td>

            </tr>
        </xsl:for-each>

    </xsl:template>

    <xsl:template match="continente[3]/paises">

        <xsl:for-each select="pais">
            <xsl:sort select="nombre" order="ascending"/>
            <tr>

                <td>
                    <img>
                        <xsl:if test="foto">
                            <xsl:attribute name="src">img/<xsl:value-of select="foto"></xsl:value-of>
                            </xsl:attribute>
                            <xsl:attribute name="alt">
                                <xsl:value-of select="nombre"></xsl:value-of>
                            </xsl:attribute>
                        </xsl:if>
                    </img>
                </td>

                <td>
                    <xsl:value-of select="nombre"/>
                </td>

                <xsl:choose>
                    <xsl:when test="nombre[@gobierno='monarquia']">
                        <td style="background-color:yellow;">
                            <xsl:value-of select="nombre/@gobierno"/>
                        </td>
                    </xsl:when>

                    <xsl:when test="nombre[@gobierno='dictadura']">
                        <td style="background-color:red;">
                            <xsl:value-of select="nombre/@gobierno"/>
                        </td>
                    </xsl:when>

                    <xsl:otherwise>
                        <td>
                            <xsl:value-of select="nombre/@gobierno"/>
                        </td>
                    </xsl:otherwise>
                </xsl:choose>

                <td>
                    <xsl:value-of select="capital"/>
                </td>

            </tr>
        </xsl:for-each>

    </xsl:template>

    <xsl:template match="continente[4]/paises">

        <xsl:for-each select="pais">
            <xsl:sort select="nombre" order="ascending"/>
            <tr>

                <td>
                    <img>
                        <xsl:if test="foto">
                            <xsl:attribute name="src">img/<xsl:value-of select="foto"></xsl:value-of>
                            </xsl:attribute>
                            <xsl:attribute name="alt">
                                <xsl:value-of select="nombre"></xsl:value-of>
                            </xsl:attribute>
                        </xsl:if>
                    </img>
                </td>

                <td>
                    <xsl:value-of select="nombre"/>
                </td>

                <xsl:choose>
                    <xsl:when test="nombre[@gobierno='monarquia']">
                        <td style="background-color:yellow;">
                            <xsl:value-of select="nombre/@gobierno"/>
                        </td>
                    </xsl:when>

                    <xsl:when test="nombre[@gobierno='dictadura']">
                        <td style="background-color:red;">
                            <xsl:value-of select="nombre/@gobierno"/>
                        </td>
                    </xsl:when>

                    <xsl:otherwise>
                        <td>
                            <xsl:value-of select="nombre/@gobierno"/>
                        </td>
                    </xsl:otherwise>
                </xsl:choose>

                <td>
                    <xsl:value-of select="capital"/>
                </td>
            </tr>
        </xsl:for-each>

    </xsl:template>

    <xsl:template match="continente[5]/paises">

        <xsl:for-each select="pais">
            <xsl:sort select="nombre" order="ascending"/>
            <tr>
                <td>
                    <img>
                        <xsl:if test="foto">
                            <xsl:attribute name="src">img/<xsl:value-of select="foto"></xsl:value-of>
                            </xsl:attribute>
                            <xsl:attribute name="alt">
                                <xsl:value-of select="nombre"></xsl:value-of>
                            </xsl:attribute>
                        </xsl:if>
                    </img>
                </td>

                <td>
                    <xsl:value-of select="nombre"/>
                </td>

                <xsl:choose>
                    <xsl:when test="nombre[@gobierno='monarquia']">
                        <td style="background-color:yellow;">
                            <xsl:value-of select="nombre/@gobierno"/>
                        </td>
                    </xsl:when>

                    <xsl:when test="nombre[@gobierno='dictadura']">
                        <td style="background-color:red;">
                            <xsl:value-of select="nombre/@gobierno"/>
                        </td>
                    </xsl:when>

                    <xsl:otherwise>
                        <td>
                            <xsl:value-of select="nombre/@gobierno"/>
                        </td>
                    </xsl:otherwise>
                </xsl:choose>
                <td>
                    <xsl:value-of select="capital"/>
                </td>

            </tr>
        </xsl:for-each>

    </xsl:template>

</xsl:stylesheet>