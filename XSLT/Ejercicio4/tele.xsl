<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" indent="yes"/>    
<xsl:template match="programacio">
        <cadena>
        <nom>Un TV</nom>

    <xsl:apply-templates select="audiencia"/>
        </cadena>
</xsl:template>

<xsl:template match="audiencia">

    <xsl:element name="programas">

            <xsl:element name="programa">
                <xsl:for-each select="hora">
                     <xsl:attribute name="hora">
                        <xsl:value-of select="."/>
                     </xsl:attribute>
                     <xsl:element name="nom-programa">
                        <xsl:value-of select="../cadenes/cadena[2]"/>
                    </xsl:element>
                

                    <xsl:element name="audiencia">
                        <xsl:value-of select="../cadenes/cadena[2]/@percentatge"/>
                    </xsl:element>
                </xsl:for-each>
      
            </xsl:element>

    </xsl:element>
</xsl:template>

</xsl:stylesheet>
