<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" indent="yes"/>    
<xsl:template match="evaluacion"> 
   <html>
       <head>
       <link rel="stylesheet" type="text/css" href="evaluacion.css" />
       </head>
       <body>
           <h2>M04 - Notas </h2>
            <table>
        <tr>
            <th>Nombre</th>
            <th>Apellidos</th>
            <th>Telefono</th>
            <th>Repetidor</th>
            <th>Nota Practica</th>
            <th>Nota Examen</th>
            <th>Nota Final</th>
            <th>Imagen</th>
        </tr>
        <xsl:apply-templates select="alumno"/>
    </table>        
       </body>
    </html>  
</xsl:template>

<xsl:template match="alumno">
        <xsl:for-each select="alumno">
        <xsl:sort select="apellido" order="descending"/>
        </xsl:for-each>

        <tr>
            <td><xsl:value-of select="nombre"/></td>
            <td><xsl:value-of select="apellidos"/></td>
            <td><xsl:value-of select="telefono"/></td>
            <td><xsl:if test="@repite = 'si'">sí</xsl:if></td>
            <xsl:apply-templates select="notas"/>

            <td>
            <img>
                <xsl:choose>
                    <xsl:when test="foto"> 
                        <xsl:attribute name="src">images/<xsl:value-of select="foto"></xsl:value-of></xsl:attribute>
                    </xsl:when>

                    <xsl:otherwise>
                        <xsl:attribute name="src">images/Ramon.jpg</xsl:attribute>
                    </xsl:otherwise>
                </xsl:choose>
                        
                        <xsl:attribute name="alt">
                            <xsl:value-of select="nombre"></xsl:value-of>
                        </xsl:attribute>

            </img>
            </td>

        </tr>
</xsl:template>

<xsl:template match="notas">
   
    <td><xsl:value-of select="practicas"/></td>
    <td><xsl:value-of select="examen"/></td>
    
    <xsl:choose>
      <xsl:when test="(practicas + examen)div 2 &gt; 8"> 
      <td style= "color:blue;"><xsl:value-of select="(practicas + examen)div(2)"/></td> 
      </xsl:when>
      <xsl:when test="(practicas + examen)div 2 &lt; 5"> 
      <td style= "color:red;"><xsl:value-of select="(practicas + examen)div(2)"/></td> 
      </xsl:when>
      <xsl:otherwise>
      <td><xsl:value-of select="(practicas + examen)div 2"/></td> 
      </xsl:otherwise>
    </xsl:choose>

</xsl:template>


</xsl:stylesheet>