function Main() {
    let main = document.createElement("main");
    document.body.appendChild(main);

    createSelect(main);
    printTable(main);
}

//Crea el select
function createSelect(main) {

    var divCentro = document.createElement("div");
    divCentro.setAttribute("class", "centro");
    main.appendChild(divCentro);

    var discografiaTitulo = document.createElement("h2");
    discografiaTitulo.textContent = "Discografía";
    divCentro.appendChild(discografiaTitulo);

    var form = document.createElement("form");
    divCentro.appendChild(form);

    var divForm = document.createElement("div");
    form.appendChild(divForm);

    var orderLabel = document.createElement("label");
    orderLabel.textContent = "Ordenar";
    divForm.appendChild(orderLabel);

    var select = document.createElement("select");
    select.setAttribute("for", "discos");
    select.setAttribute("id", "discos");
    select.setAttribute("onchange", "reorder()");
    divForm.appendChild(select);

    var optionName = document.createElement("option");
    optionName.setAttribute("value", "nombre");
    optionName.textContent = "Nombre";
    select.appendChild(optionName);

    var optionInterprete = document.createElement("option");
    optionInterprete.setAttribute("value", "nombreArtistico");
    optionInterprete.textContent = "Intérprete";
    select.appendChild(optionInterprete);

    var optionGenero = document.createElement("option");
    optionGenero.setAttribute("value", "genero");
    optionGenero.textContent = "Género";
    select.appendChild(optionGenero);

    var optionAnyo = document.createElement("option");
    optionAnyo.setAttribute("value", "anyoPublicacion");
    optionAnyo.textContent = "Año";
    select.appendChild(optionAnyo);
}

//Crea el html de los discos
function printTable() {

    let section = document.createElement("section");
    let divCentro = document.querySelector(".centro");
    divCentro.appendChild(section);

    for (const disco of discography.listaDiscos) {

        var divContenedor = document.createElement("div");
        divContenedor.setAttribute("class", "contenedor");
        section.appendChild(divContenedor);

        var imgDelete = document.createElement("img");
        imgDelete.setAttribute("src", "images/Delete.png")
        imgDelete.setAttribute("onclick", "discography.deleteDisco()")
        imgDelete.setAttribute("class", "delete");
        divContenedor.appendChild(imgDelete);

        var divCaracteristicas = document.createElement("div");
        divCaracteristicas.setAttribute("class", "caracteristicas");
        divContenedor.appendChild(divCaracteristicas);

        var nombreP = document.createElement("p");
        nombreP.textContent = "Nombre: " + disco.nombre;
        divCaracteristicas.appendChild(nombreP);

        var interpreteP = document.createElement("p");
        interpreteP.textContent = "Interprete: " + disco.interprete.nombreArtistico;
        divCaracteristicas.appendChild(interpreteP);

        var generoP = document.createElement("p");
        generoP.textContent = "Género: " + disco.genero;
        divCaracteristicas.appendChild(generoP);

        var anyoP = document.createElement("p");
        anyoP.textContent = "Año de publicación: " + disco.anyoPublicacion;
        divCaracteristicas.appendChild(anyoP);

        var imagen = document.createElement("img");
        imagen.setAttribute("src", disco.imagen)
        imagen.setAttribute("alt", "Imagen del disco")
        divContenedor.appendChild(imagen);
    }

    var divAddImage = document.createElement("div");
    divAddImage.setAttribute("class", "contenedor")
    divAddImage.setAttribute("id", "addImage");
    section.appendChild(divAddImage);

    var imagen = document.createElement("img");
    imagen.setAttribute("src", "images/AddSong.png")
    imagen.setAttribute("onclick", "changeImageDOM()")
    imagen.setAttribute("class", "add")
    divAddImage.appendChild(imagen);
}

//Imprime los elementos según el orden de select
function reorder() {

    let valor = document.querySelector("select").value;

    discography.sortAttributes(valor);

    let section = document.querySelector("section");

    section.remove();

    printTable();
}

//Cambia la imagen de añadir disco por el formulario
function changeImageDOM() {
    var imagen = document.querySelector(".add")
    var divImage = document.querySelector("#addImage")
    imagen.remove();

    var form = document.createElement("form");
    divImage.appendChild(form);

    var divInput = document.createElement("div");
    divInput.setAttribute("class", "input");
    form.appendChild(divInput);

    var nameInput = document.createElement("input");
    nameInput.setAttribute("type", "text");
    nameInput.setAttribute("id", "name");
    nameInput.setAttribute("name", "name");
    nameInput.setAttribute("placeholder", "Introduce el nombre del disco");
    divInput.appendChild(nameInput);

    var nombreInterpreteInput = document.createElement("input");
    nombreInterpreteInput.setAttribute("type", "text");
    nombreInterpreteInput.setAttribute("id", "nInterprete");
    nombreInterpreteInput.setAttribute("name", "nInterprete");
    nombreInterpreteInput.setAttribute("placeholder", "Introduce el nombre del intérprete");
    divInput.appendChild(nombreInterpreteInput);

    var apellidoInterpreteInput = document.createElement("input");
    apellidoInterpreteInput.setAttribute("type", "text");
    apellidoInterpreteInput.setAttribute("id", "aInterprete");
    apellidoInterpreteInput.setAttribute("name", "aInterprete");
    apellidoInterpreteInput.setAttribute("placeholder", "Introduce el apellido del intérprete");
    divInput.appendChild(apellidoInterpreteInput);

    var nombreArtisticoInterpreteInput = document.createElement("input");
    nombreArtisticoInterpreteInput.setAttribute("type", "text");
    nombreArtisticoInterpreteInput.setAttribute("id", "nAInterprete");
    nombreArtisticoInterpreteInput.setAttribute("name", "nAInterprete");
    nombreArtisticoInterpreteInput.setAttribute("placeholder", "Introduce el nombre artístico del intérprete");
    divInput.appendChild(nombreArtisticoInterpreteInput);

    var generoInput = document.createElement("input");
    generoInput.setAttribute("type", "text");
    generoInput.setAttribute("id", "genero");
    generoInput.setAttribute("name", "genero");
    generoInput.setAttribute("placeholder", "Introduce el género");
    divInput.appendChild(generoInput);

    var anyoInput = document.createElement("input");
    anyoInput.setAttribute("type", "text");
    anyoInput.setAttribute("id", "anyo");
    anyoInput.setAttribute("name", "anyo");
    anyoInput.setAttribute("placeholder", "Introduce el año de publicación");
    divInput.appendChild(anyoInput);

    var imagenInput = document.createElement("input");
    imagenInput.setAttribute("type", "text");
    imagenInput.setAttribute("id", "img");
    imagenInput.setAttribute("name", "img");
    imagenInput.setAttribute("placeholder", "Introduce una URL");
    divInput.appendChild(imagenInput);

    var buttonSubmit = document.createElement("button");
    buttonSubmit.setAttribute("type", "button");
    buttonSubmit.setAttribute("onclick", "discography.pushDiskToDOM()");
    buttonSubmit.textContent = "Publicar disco";
    divInput.appendChild(buttonSubmit);

}