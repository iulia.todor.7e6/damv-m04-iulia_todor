class Discografia {
    listaDiscos = [];

    addDisco(disk) {
        this.listaDiscos.push(disk);
    }

    //añade el disco al DOM
    pushDiskToDOM(disk) {
        let nameLabel = document.querySelector('input[name="name"]').value;
        let nInterpreteLabel = document.querySelector('input[name="nInterprete"]').value;
        let aInterpreteLabel = document.querySelector('input[name="aInterprete"]').value;
        let nAInterpreteLabel = document.querySelector('input[name="nAInterprete"]').value;
        let anyoLabel = document.querySelector('input[name="anyo"]').value;
        let generoLabel = document.querySelector('input[name="genero"]').value;
        let imgLabel = document.querySelector('input[name="img"]').value;
        
        var section = document.querySelector("section");
        const interprete = new Interprete(nInterpreteLabel, aInterpreteLabel, nAInterpreteLabel);
        disk = new Disco();
        disk.includeProperties(nameLabel, interprete, anyoLabel, generoLabel, imgLabel);
        discography.addDisco(disk);

        section.remove();

        printTable();
    }

    //Solo elimina el elemento de arriba en la página. No conseguí parametrizarlo.
    deleteDisco(disk) {
        let newListaDiscos = discography.listaDiscos.splice(disk, 1)
        let section = document.querySelector("section");

        section.remove();

        printTable();
    }

    sortAttributes(attribute) {
        if (attribute == "nombreArtistico") {
            this.listaDiscos.sort(
                (a, b) => {
                    if (a["interprete"][attribute] < b["interprete"][attribute]) {
                        return -1;
                    }
                    if (a["interprete"][attribute] > b["interprete"][attribute]) {
                        return 1;
                    }
                    return 0;
                }
            );
        }
        else {
            this.listaDiscos.sort(
                (a, b) => {
                    if (a[attribute] < b[attribute]) {
                        return -1;
                    }
                    if (a[attribute] > b[attribute]) {
                        return 1;
                    }
                    return 0;
                }
            );
        }
    }

    returnAllExceptImage() {
        var text = "";
        for (let disco of this.listaDiscos) {
            //Para lo de interprete: Primero accedemos a los objetos. Luego entramos a interprete con GetInterprete. Luego imprimimos los datos de Interprete con GetFullInterprete.
            text += disco.nombre + disco.interprete().getFullInterprete() + disco.anyoPublicacion + disco.genero + disco.localizacion() + disco.prestado();
        }
        return text;
    }

}