const pinkFloyd = new Interprete("Roger Keith", "Barret", "Pink Floyd");
const discography = new Discografia();
const discoPinkFloyd = new Disco();
discoPinkFloyd.includeProperties("The Dark Side of the Moon", pinkFloyd, 1972, "Rock", "images/DarkSide.png");
discography.addDisco(discoPinkFloyd);

const skillet = new Interprete("John", "Cooper", "Skillet");
const discoUnleashed = new Disco();
discoUnleashed.includeProperties("Unleashed", skillet, 2016, "Rock", "images/Unleashed.jpg");
discography.addDisco(discoUnleashed);

const vanGogh = new Interprete("Pablo", "Benegas", "La oreja de Van Gogh");
const discoRosas = new Disco();
discoRosas.includeProperties("Rosas", vanGogh, 2003, "Pop", "images/Rosas.png");
discography.addDisco(discoRosas);

const jamiroquai = new Interprete("Jay", "Kay", "Jamiroquai");
const discoDynamite = new Disco();
discoDynamite.includeProperties("Dynamite", jamiroquai, 2005, "Funk", "images/Dynamite.jpg");
discography.addDisco(discoDynamite);

const metalica = new Interprete("Lars", "Ulrich", "Metallica");
const discoRTL = new Disco();
discoRTL.includeProperties("Ride the Lightning", metalica, 1984, "Heavy Metal", "images/RideTheLightning.jpg");
discography.addDisco(discoRTL);