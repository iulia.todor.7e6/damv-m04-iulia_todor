class Interprete {
    #nombre;
    #apellido;
    #nombreArtistico;

    get nombre() {
        return this.#nombre;
    }

    get apellido() {
        return this.#apellido;
    }

    get nombreArtistico() {
        return this.#nombreArtistico;
    }

    getFullInterprete() {
        return "Nombre: " + this.#nombre + ' ' + "Apellido" + this.#apellido + ' ' + "Nombre artístico: " + this.#nombreArtistico;
    }

    constructor(nombre, apellido, nombreArtistico) {
        this.#nombre = nombre;
        this.#apellido = apellido;
        this.#nombreArtistico = nombreArtistico;
    }

}





