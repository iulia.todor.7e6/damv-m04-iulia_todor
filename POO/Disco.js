class Disco {
    nombre;
    interprete;
    anyoPublicacion;
    genero;
    imagen;
    #localizacion;
    #prestado;

    get localizacion() {
        return this.#localizacion;
    }

    get prestado() {
        return this.#prestado;
    }

    constructor() {
        this.nombre = "";
        this.anyoPublicacion = "";
        this.genero = "";
        this.imagen = "";
        this.#localizacion = 0;
        this.#prestado = false;
    }

    includeProperties(nombre, interprete, anyoPublicacion, genero, imagen) {
        this.nombre = nombre;
        this.interprete = interprete;
        this.anyoPublicacion = anyoPublicacion;
        this.genero = genero;
        this.imagen = imagen;
        this.#localizacion = 0;
        this.#prestado = false;
    }

    getInfo() {
        return "Nombre: " + this.nombre + ' ' + "Interprete: " + this.interprete.getFullInterprete() + ' ' + "Año publicación: " + this.anyoPublicacion + ' ' + "Género: " + this.genero + ' ' + "Imagen: " + this.imagen + ' ' + "Idioma: " + this.#localizacion + ' ' + "Prestado: " + this.#prestado;
    }

}