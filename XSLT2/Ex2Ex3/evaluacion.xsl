<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" indent="yes"/>    
<xsl:template match="evaluacion">
    <html>
        <head>
        <link rel="stylesheet" type="text/css" href="evaluacion.css"/>
        </head>   

        <body> 
            <table>
                <tr>
                    <th>Nombre</th>
                    <th>Apellido</th>
                    <th>Telefono</th>
                    <th>Repetidor</th>
                    <th>Nota práctica</th>
                    <th>Nota examen</th>
                    <th>Nota final</th>
                    <th>Foto</th>
                </tr>
                <xsl:apply-templates select="alumno"/>
            </table>
        </body>
    
    </html>    
</xsl:template>

<xsl:template match="alumno">
<xsl:for-each select=".">
<xsl:sort select="apellidos" order="descending"/>
<tr>
<td><xsl:value-of select="nombre"/></td>
<td><xsl:value-of select="apellidos"/></td>
<td><xsl:value-of select="telefono"/></td>
<td>
    <xsl:if test="@repite = 'si'">
        <xsl:value-of select="@repite"/>
    </xsl:if>
</td>
<xsl:apply-templates select="notas"/>

<td>
<img>
<xsl:choose>
<xsl:when test="foto">
<xsl:attribute name="src">
images/<xsl:value-of select="foto"/></xsl:attribute>
</xsl:when>

<xsl:otherwise>
<xsl:attribute name="src">images/Default.jpg</xsl:attribute>
</xsl:otherwise>
</xsl:choose>
</img>
</td>
</tr>
</xsl:for-each>

</xsl:template>

<xsl:template match="notas">
<td><xsl:value-of select="practicas"/></td>
<td><xsl:value-of select="examen"/></td>
<xsl:variable name="NotaFinal" select="(practicas + examen)div 2"/>

<xsl:choose>
<xsl:when test="$NotaFinal &lt; 5">
<td style="color:red;"><xsl:value-of select="$NotaFinal"/></td>
</xsl:when>

<xsl:when test="$NotaFinal &gt; 8">
<td style="color:blue;"><xsl:value-of select="$NotaFinal"/></td>
</xsl:when>

<xsl:otherwise>
<td><xsl:value-of select="$NotaFinal"/></td>
</xsl:otherwise>
</xsl:choose>




</xsl:template>

</xsl:stylesheet>