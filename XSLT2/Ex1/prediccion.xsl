<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" indent="yes"/>    
<xsl:template match="root">
    <html>
        <head>
        <link rel="stylesheet" type="text/css" href="prediccion.css"/>
        </head>   

        <body> 
            <table>
                <tr>
                    <td>Fecha</td>
                    <td>Maxima</td>
                    <td>Minima</td>
                    <td>Prediccion</td>
                </tr>
                <xsl:apply-templates select="prediccion"/>
            </table>
        </body>

    </html>    
</xsl:template>

<xsl:template match="prediccion">
<xsl:for-each select="dia">
<tr>
    <td>
    <xsl:value-of select="@fecha"/>
    </td>
    <td>
    <xsl:value-of select="temperatura/maxima"/>
    </td>
    <td>
    <xsl:value-of select="temperatura/minima"/>
    </td>
    <td>
        <img>
            <xsl:if test="foto">
                <xsl:attribute name="src">images/<xsl:value-of select="foto"></xsl:value-of>
                </xsl:attribute>
                <xsl:attribute name="alt"><xsl:value-of select="estado_cielo[@periodo=00-24]/@descripcion"></xsl:value-of>
                </xsl:attribute>
            </xsl:if>
        </img>
    </td>
</tr>
</xsl:for-each>
</xsl:template>

</xsl:stylesheet>